# Need for speed #

Fa pocs dies els mitjans de comunicació han informat que una persona famosa (les inicials de la qual són EA) ha comès diverses infraccions de trànsit en aparcar el cotxe en el carril bus. Malgrat els agents de la guàrdia urbana l'han avisat i multat, la persona en qüestió ha fugit amb el seu cotxe (després de tirar a terra una moto d'un dels agents). A partir d'aquí, els agents han iniciat una persecució pels carrers de la ciutat per tal de detenir l'EA. 

Com que durant la persecució l'EA ha sobrepassat més d'una vegada el límit de velocitat (segons han enregistrat els diferents radars), et demanen que facis un programa que permeti saber el nom dels carrers en què s'ha superat la velocitat mitjana enregistrada entre tots els carrers. 

L'**entrada del programa** estarà formada per una primera línia amb N valors (com a mínim 1 i fins a un màxim de 231), cadascun entre 1 i 400, que indicaran les velocitats enregistrades per la fugitiva en els diferents carrers per on hagi passat. A continuació hi haurà tantes línies com valors s'hagin llegit en la primera línia. En cadascuna d'aquestes línies hi haurà un nom de carrer. El carrer llegit en cada línia correspon al carrer on s'ha enregistrat cada valor. És a dir, el primer valor de la línia on hi ha els valors serà el que correspondrà al carrer que apareix a la primera línia de carrers, el segon valor serà el del segon carrer, etc.

La **sortida del programa** estarà formada per un número que serà la mitjana de velocitats que s'ha de calcular. A més, també caldrà escriure NO S'HAN TROBAT CARRERS en el cas que no hi hagi cap carrer en què s'hagi superat la mitjana, o bé el nom de tots els carrers (un a cada línia) que superin la mitjana de les velocitats enregistrades.

## Exemple d'entrada
>		4
>		250 49 75 130 
>		Carrer Gas a Fons 
>		Carrer Compte que Rius 
>		Via Catalana 
>		Passatge Nova Onada

## Exemple de sortida
>		126.0
>		Carrer Gas a Fons
>		Passatge Nova Onada
