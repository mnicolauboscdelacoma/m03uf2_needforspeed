package cat.boscdelacoma.mp03.uf2.needforspeed;

import java.util.Scanner;

/**
 *
 * @author Marc Nicolau
 */
public class NeedForSpeed {

    static Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        int[] velocitats = llegirVelocitats();
        String[] carrers = llegirCarrers(velocitats.length);
        float mitjana = calcularMitjana(velocitats);

        System.out.println(mitjana);
        mostrarCarrersSobreMitjana(carrers, velocitats, mitjana);
    }

    /**
     * llegeix les velocitat que arriben per l'entrada.
     * 
     * @return 
     */
    static int[] llegirVelocitats() {
        int nVelocitats = Integer.parseInt(lector.nextLine());
        String[] linia = lector.nextLine().split(" ");
        int[] vel = new int[nVelocitats];
        int i = 0;

        for (String s : linia) {
            vel[i++] = Integer.parseInt(s);
        }
        return vel;
    }

    /**
     * Donat un array de velocitats, en calcula la seva mitjana.
     * 
     * @param velocitats L'array que conté les velocitats.
     * @return La velocitat mitjana.
     */
    static float calcularMitjana(int[] velocitats) {
        float avg = 0;
        for (int v : velocitats) {
            avg += v;
        }
        return avg / velocitats.length;
    }

    /**
     * Llegeix el nom de carrers, a partir d'una quantitat indicada.
     * 
     * @param n La quantitat de noms de carrers que s'han de llegir.
     * @return Array que conté els noms dels carrers que s'han llegit.
     */
    static String[] llegirCarrers(int n) {
        String[] carrers = new String[n];
        for (int i = 0; i < n; i++) {
            carrers[i] = lector.nextLine();
        }
        return carrers;
    }

    /**
     * Mostra el nom dels carrers que estan per sobre la mitjana de velocitats.
     * 
     * @param carrers Array que conté els noms dels carrers
     * @param velocitats Array que conté les velocitats màximes dels carrers.
     * @param mitjana El valor de la velocitat mitjana.
     */
    static void mostrarCarrersSobreMitjana(String[] carrers, int[] velocitats,
            float mitjana) {

        boolean trobat = false;

        for (int i = 0; i < velocitats.length; i++) {
            if (velocitats[i] > mitjana) {
                System.out.println(carrers[i]);
                trobat = true;
            }
        }
        if (!trobat) {
            System.out.println("NO S'HAN TROBAT CARRERS");
        }
    }

}
